function findClevercastResource(entry) {
    return entry.name.startsWith("https://player.clevercast.com/");
}

var clevercastUrl = performance.getEntriesByType("resource").find(findClevercastResource).name;
var idIndex = clevercastUrl.indexOf("event_id=") + 9;
var id = clevercastUrl.slice(idIndex, idIndex + 6);
var m3u8Url = "https://hls-origin01-uugent.cdn01.rambla.be/main/adliveorigin-uugent/_definst_/" + id + ".stream/chunklist.m3u8";
var titleColumn = document.getElementsByClassName("row main")[0].getElementsByClassName("col-md-12")[0];
// Set title display to inline
titleColumn.firstElementChild.style.display = 'inline';
// Add link to m3u8
titleColumn.innerHTML += "<a href=\"" + m3u8Url + "\" class=\"btn btn-primary pull-right new-lesopname mb-4\">m3u8</a>";
